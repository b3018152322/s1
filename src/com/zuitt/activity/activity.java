package com.zuitt.activity;

import java.util.Scanner;
public class activity {

    public static void main(String args[]){
        Scanner category = new Scanner(System.in);

        System.out.println("Please enter your first name: ");
        String first_name = category.nextLine();

        System.out.println("Please enter your last name: ");
        String last_name = category.nextLine();

        System.out.println("Please enter your grade in your first subject: ");
        double firstSubject_grade = new Double(category.nextLine());

        System.out.println("Please enter your grade in your second subject: ");
        double secondSubject_grade = new Double(category.nextLine());

        System.out.println("Please enter your grade in your third subject: ");
        double thirdSubject_grade = new Double(category.nextLine());

        double average = (firstSubject_grade + secondSubject_grade + thirdSubject_grade)/3;
        int roundAverage = (int)average;

        System.out.println("Good day, " + first_name + " " + last_name + ".");
        System.out.println("Your average grade is: " + roundAverage + ".");

    }

}
